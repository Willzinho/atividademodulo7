public class contaCorrente extends Conta{
	
	private double chequeEspecial;
	
	public contaCorrente(int numero, int agencia, String banco, double saldo, double deposito, double saque, double chequeEspecial) {
		super(numero, agencia, banco, saldo, deposito, saque);
		this.chequeEspecial = chequeEspecial;
	}

	@Override
		public String toString() {
		return "contaCorrente{" + 
				"chequeEspecial=" + chequeEspecial +
				'}';
	}
	
	public double getSaldo() {
		return this.chequeEspecial + this.saldo + this.deposito - this.saque;
	}

	@Override
	public double getDepositar() {
		return this.deposito;
	}

	@Override
	public double getSacar() {
		if (this.saque <= this.chequeEspecial + this.deposito) {
			return this.saque;
		}
		else {
			return this.saque = 0;
		}
		}
		}