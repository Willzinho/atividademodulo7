public class ContaSalario extends Conta {
	
	private int limiteSaque;
	
	public ContaSalario(int numero, int agencia, String banco, double saldo, double deposito, double saque, int limiteSaque) {
		super(numero, agencia, banco, saldo, deposito, saque);
		this.limiteSaque = limiteSaque;
	}
	
	public int getlimiteSaque() {
		return limiteSaque;
	}
	
	public void setlimiteSaque(int limiteSaque) {
		this.limiteSaque = limiteSaque;
	}

	public double getSaldo() {
		return this.saldo + getDepositar() - getSacar();
		
	}
	
	@Override
	public double getDepositar() {
		return this.deposito;
	}
	
	@Override
	
	public double getSacar() {
		if (limiteSaque >= 3) {
			
			System.out.println(" Você passou do limite de saques da sua conta salário \n");
			return this.saque = 0;}
		else {
		 return this.saque;
        } 
	}
	
	
		
		
		
	}
			
